Grunt Angular Auto Dependencies
===============================

Automatically add dependencies to angular modules.

#### Open/Close Tags:
```js
var dependencies = [
  // #AUTO_DEPENDENCY
  // dependencies will be placed here
  // #END_AUTO_DEPENDENCY
];
```

#### Example Configuration
```js
gruntConfig.autoDependencies = {
  app: {
    options: {
      // default options
      appendComma: false,
      indent: 2,
      openTag: '// #AUTO_DEPENDENCY',
      closeTag: '// #END_AUTO_DEPENDENCY'
    },
    files: {
      'src/app.js': [
        'src/app/**/*.js',
        '!src/app/**/*.controller.js'
      ]
    }
  }
};
```