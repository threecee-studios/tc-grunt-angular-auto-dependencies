module.exports = function(grunt) {
  'use strict';

  grunt.registerMultiTask('autoDependencies', 'calculates app dependencies', function() {

    var done = this.async();
    var fs = require( 'fs' );
    var replaceBetweenTags = require( 'tc-replace-between-tags' );

    var options = this.options({
      indent: 2,
      base: 'src/',
      openTag: '// #AUTO_DEPENDENCY',
      closeTag: '// #END_AUTO_DEPENDENCY',
      appendComma: false
    });

    var space = '';
    for (var i = 0; i < options.indent; i++) {
      space += ' ';
    }

    var remaining = this.files.length;

    this.files.forEach( function( file ) {

      grunt.log.writeln('Adding Dependencies To: ' + file.dest);
      grunt.log.writeln('Dependencies: ' + file.src.join(' + '));

      var modules = file.src.filter( function( filepath ) {
        if (!grunt.file.exists(filepath)) {
          grunt.log.warn( 'Module "' + filepath + '" not found.' );
          return false;
        } else {
          return true;
        }
      }).map( function( filepath ) {
        if (grunt.file.isDir(filepath)) {
          return;
        }
        var moduleName = '';
        if ( filepath.indexOf( options.base ) === 0) {
          moduleName += filepath.substr( options.base.length, filepath.length);
        } else {
          moduleName += filepath;
        }
        moduleName = moduleName.substr(0, moduleName.lastIndexOf('/') ).replace(/\//g, '.');
        return moduleName;
      });

      var modulesString = space + '\'' + modules.join( '\',\n' + space + '\'' ) + '\'';
      if ( options.appendComma ) {
        modulesString += ',';
      }
      modulesString += '\n';

      fs.readFile( file.dest, 'utf8', function( error, fileContents ) {
        if ( error ) {
          grunt.log.error( error );
          if ( --remaining <= 0 ) {
            done();
          }
        } else {
          var content = replaceBetweenTags(options.openTag, options.closeTag, modulesString, fileContents);
          if ( content ) {
            fs.writeFile( file.dest, content, function ( error ) {
              if ( error ) {
                grunt.log.error( error );
                if ( --remaining <= 0 ) {
                  done();
                }
              } else {
                if ( --remaining <= 0 ) {
                  done();
                }
              }
            });
          } else {
            grunt.log.error( 'Could not find opening or closing tags' );
            if ( --remaining <= 0 ) {
              done();
            }
          }
        }
      });
    });

  });

};
